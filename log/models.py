from django.db import models

# Create your models here.
class Log(models.Model):
	email = models.CharField(max_length = 100, null = False, default = "asdasd@gmail.com")
	id_item = models.IntegerField(null = False, default = 1)
	quantity = models.CharField(max_length = 200, null = False, default = 1)
	log = models.CharField(max_length = 1000, null = False)
