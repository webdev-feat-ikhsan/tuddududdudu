from rest_framework import serializers
from . import models

class LogSerializer(serializers.ModelSerializer):
	class Meta:
	    model = models.Log
	    fields = ('id', 'log')
