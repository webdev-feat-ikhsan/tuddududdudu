from django.db import models

# Create your models here.
class Item(models.Model):
    id = models.IntegerField(primary_key=True)
    quantity = models.IntegerField(default=1000)
