from django.shortcuts import render
from django.core import serializers as ser
from rest_framework import viewsets, status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from . import models
from . import serializers

# Create your views here.
class ItemViewset(viewsets.ModelViewSet):
    queryset = models.Item.objects.all()
    serializer_class = serializers.ItemSerializer

    def create(self, serializer):
        item_id = serializer.data['id']
        item, created = models.Item.objects.get_or_create(id=item_id)
        item.quantity = item.quantity - 1
        if not item.quantity:
            item.quantity = 1000
        item.save()
        res = {}
        res["id"] = item.id
        res["quantity"] = item.quantity
        headers = self.get_success_headers(serializer.data)
        return Response(res, status=status.HTTP_201_CREATED, headers=headers)