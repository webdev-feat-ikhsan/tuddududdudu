from rest_framework import routers
from todo import views as todo_views
from user import views as user_views
from item import views as item_views
from log import views as log_views

router = routers.DefaultRouter()
router.register(r'todos', todo_views.ToDoViewset)
router.register(r'users', user_views.UserViewset)
router.register(r'items', item_views.ItemViewset)
router.register(r'logs', log_views.LogViewset)
