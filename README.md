# tuddududdudu

Hit you with that tuddududdudu~

1. Install docker, minikube, kubectl
2. Jalanin `minikube start`
3. Copy ini ke terminal `eval $(minikube docker-env)`
4. Di root project ini jalanin `docker build -t blekping-docker-image .`
5. `kubectl apply -f deploy/kubernetes/item/postgres/volume.yaml`
6. `kubectl apply -f deploy/kubernetes/item/postgres/volume_claim.yaml`
7. `kubectl apply -f deploy/kubernetes/item/postgres/secrets.yaml`
8. `kubectl apply -f deploy/kubernetes/item/postgres/deployment.yaml`
9. `kubectl apply -f deploy/kubernetes/item/postgres/service.yaml`
10. `kubectl apply -f deploy/kubernetes/log/postgres/volume.yaml`
11. `kubectl apply -f deploy/kubernetes/log/postgres/volume_claim.yaml`
12. `kubectl apply -f deploy/kubernetes/log/postgres/secrets.yaml`
13. `kubectl apply -f deploy/kubernetes/log/postgres/deployment.yaml`
14. `kubectl apply -f deploy/kubernetes/log/postgres/service.yaml`
15. `kubectl apply -f deploy/kubernetes/user/postgres/volume.yaml`
16. `kubectl apply -f deploy/kubernetes/user/postgres/volume_claim.yaml`
17. `kubectl apply -f deploy/kubernetes/user/postgres/secrets.yaml`
18. `kubectl apply -f deploy/kubernetes/user/postgres/deployment.yaml`
19. `kubectl apply -f deploy/kubernetes/user/postgres/service.yaml`
20. Buka `minikube dasboard` tunggu sampe ga ada merah merah lagi (failed)
21. `kubectl apply -f deploy/kubernetes/item/django/deployment.yaml`
22. `kubectl apply -f deploy/kubernetes/item/django/service.yaml`
23. `kubectl apply -f deploy/kubernetes/item/django/job-migration.yaml`
24. `kubectl apply -f deploy/kubernetes/log/django/deployment.yaml`
25. `kubectl apply -f deploy/kubernetes/log/django/service.yaml`
26. `kubectl apply -f deploy/kubernetes/log/django/job-migration.yaml`
27. `kubectl apply -f deploy/kubernetes/user/django/deployment.yaml`
28. `kubectl apply -f deploy/kubernetes/user/django/service.yaml`
29. `kubectl apply -f deploy/kubernetes/user/django/job-migration.yaml`
30. `kubectl apply -f deploy/kubernetes/transaction/django/deployment.yaml`
31. `kubectl apply -f deploy/kubernetes/transaction/django/service.yaml`
32. `kubectl apply -f deploy/kubernetes/transaction/django/job-migration.yaml`
33. Coba jalanin `minikube service blekping-kube-deployment` harusnya kebuka app django di browser.
