from django.shortcuts import render
from django.http import HttpResponse
import requests
import json
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
def create_log(user_email, item_id):
    return "Customer "+user_email+" purchased item with id "+str(item_id)


@csrf_exempt
def transaction(request):
    data = json.loads(request.body.decode('utf-8'))
    item_id = data["item_id"]
    user_email = data["user_email"]
    log = create_log(user_email, item_id)
    item_res = requests.post("http://blekping-kube-deployment-item.default.svc.cluster.local/api/v1/items/", data = {'id': item_id}) #Item
    user_res = requests.post("http://blekping-kube-deployment-user.default.svc.cluster.local/api/v1/users/", data = {'email': user_email}) #User
    log_res = requests.post("http://blekping-kube-deployment-log.default.svc.cluster.local/api/v1/logs/", data = {'log': log}) #Log
    return HttpResponse(item_res)
